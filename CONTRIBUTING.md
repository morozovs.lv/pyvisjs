# Contributing to pyvisjs

Welcome to pyvisjs! We're glad you're interested in contributing. Please take a moment to review the following guidelines to ensure a smooth and effective contribution process.

## How to Contribute

We welcome contributions in the following areas:

- Reporting bugs
- Submitting feature requests
- Writing code improvements
- Providing documentation updates

Please follow these steps when contributing:

- **Create an Issue (Optional)**: Before making changes, consider creating an issue (Plan -> Issues -> New issue) to discuss and track the proposed changes. This step helps ensure that your contributions align with the project's goals and can provide helpful context for your merge request.

- **Clone the Repository**: Clone the repository to your local machine using the ```git clone``` command.

- **Create a Branch**: Create a new branch for your changes using the ```git checkout -b branch-name``` command, replacing branch-name with a descriptive name for your branch.

- **Make Changes**: Make your desired changes to the codebase, committing them to your branch with ```git commit```.

- **Push Changes**: Push your changes to the repository on GitLab using ```git push origin branch-name```.

- **Create Merge Request**: Navigate to the project repository and switch to the branch you pushed your changes to. Click on the "Merge Requests" tab and then on the "New merge request" button.

- **Select Source and Target Branches**: Choose the branch containing your changes as the "source branch" and ```dev``` branch as the "target branch."

- **Fill in Merge Request Details**: Provide a title and description for your merge request, detailing the changes you made and why they are necessary.

- **Submit Merge Request**: Once you're satisfied with your changes and the merge request details, submit the merge request.

- **Monitor and Respond to Feedback**: Keep an eye on your merge request for any comments or feedback from project maintainers. Make any requested changes and update the merge request accordingly.

- **Merge Changes**: Once your merge request has been approved, a project maintainer will merge your changes into the main branch of the repository.


## Do not forget
> you can use the following keywords followed by #issue_number in your commit message or merge request subject! 

- Close, Closes, Closed, Closing, close, closes, closed, closing

- Fix, Fixes, Fixed, Fixing, fix, fixes, fixed, fixing
- Resolve, Resolves, Resolved, Resolving, resolve, resolves, resolved, resolving

for example ```This commit is also related to #17 and fixes #18, #19```


## The git part from above in short
```
git clone https://gitlab.com/22kittens/pyvisjs.git
git checkout dev
git checkout -b feature-name
git add .
git commit -m "Fixes issue #12"
git push origin feature-name
<Create a Pull Request>

```

## Community Guidelines

We value a respectful and inclusive community. Please adhere to the following guidelines when interacting with others in the pyvisjs community:

- Be respectful and considerate of others' opinions and contributions.
- Avoid offensive language, harassment, or discrimination of any kind.
- Help create a welcoming and inclusive environment for all community members.

## Contact

If you have any questions or need assistance, feel free to contact the project owner at andrey@morozov.lv.

Thank you for contributing to pyvisjs! We appreciate your support and contributions.