__major__ = "0"
__minor__ = "0"
__patch__ = "0dev3"

__version__ = f"{__major__}.{__minor__}.{__patch__}"